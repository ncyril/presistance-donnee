from flask import Flask, redirect, render_template, request
from sqlalchemy import ForeignKey, create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DATABASE_URL = 'postgresql://postgres:password@172.17.0.1:5435/postgres'

engine = create_engine(DATABASE_URL)

Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(String)

    def __repr__(self):
        return f"<User(name='{self.name}', email='{self.email}')>"
    
class Address(Base):
    __tablename__ = 'addresses'

    id = Column(Integer, primary_key=True)
    street = Column(String)
    zipcode = Column(String)
    city = Column(String)
    
    user_id = Column(Integer, ForeignKey('users.id'))

    def __repr__(self):
        return f"<Address(street='{self.street}', zipcode='{self.zipcode}', city='{self.city}')>"    

Base.metadata.create_all(engine)
app = Flask(__name__)


@app.route('/', methods=['GET'])
def render_html_list():
    users = session.query(User, Address).join(Address, User.id == Address.user_id).all()
    print(users)

    return render_template('users.html', users=users)

@app.route('/post', methods=['POST'])
def post_users_info():
    name = request.form['name']    
    email = request.form['email']    
    street = request.form['street']    
    zipcode = request.form['zipcode']    
    city = request.form['city']
    
    new_user = User(name=name, email=email)
    session.add(new_user)
    session.flush()
    
    new_address = Address(street=street, zipcode=zipcode, city=city, user_id=new_user.id)
    session.add(new_address)
    session.commit()
    
    return redirect('/')
  
if __name__ == '__main__':
    app.run(debug=True)
